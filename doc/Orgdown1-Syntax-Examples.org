★ [[Overview.org][← Overview page]] ★

* Orgdown1 Syntax Examples

There is currently one initial [[Orgdown-Levels.org][level for Orgdown]] which is named *Orgdown1*.

It covers the most basic set of syntax elements that any tool has to
support in order to support Orgdown.

Here are all basic syntax elements of Orgdown1. If you want to *learn
the Orgdown syntax* you should better start with [[Learning-Orgdown.org][the tutorial page]].

** *Simple* /text/ _formatting_

- *bold*
- /italic/
- _underline_
- +strike through+
- =code=
- ~commands~

: small example

Within links:

[[https://gitlab.com/publicvoit/orgdown][This *is* an /example/ of _some_ syntax +highlighting+ within =links= and ~such~.]]

[[https://gnu.org][~gnu~]]

-----

Five or more dashes produce a horizontal line.

** Headings

Headings are pre-pended by a set of asterisk characters followed by at
least one space character.

One asterisk is a heading of level 1, two asterisks for level 2 and so forth:

: * Heading of level 1
: ** Heading of level 2
: *** Heading of level 3
: *** Another heading of level 3
: ** Again a heading of level 2

** Lists and Checkboxes

simple list:
- Org-mode
- Lists
- [ ] unchecked checkbox
- [X] checked checkbox
- [-] undecided checkbox (=C-u C-u C-c C-c=)

enumerate:
1. Emacs
2. Org-mode
3. Lists

mixed with checkboxes:

- Clean out garage
  1. [ ] get stuff out
     - [X] be careful with that axe, Eugene
  2. [X] get rid of old stuff
     - sell on the Internet?
     - try to use rubbish as birthday presents for family
  3. [ ] repaint garage
  4. [ ] put stuff back in

** Blocks

An EXAMPLE block is rendered "as is", keeping line breaks and not
interpreting content:

#+BEGIN_EXAMPLE
An example in an EXAMPLE block.
Second line within this block.

This *is* an /example/ of _some_ syntax +highlighting+.
#+END_EXAMPLE

Content within a QUOTE block may get different line breaks when
exported/rendered and interprets Orgdown text formatting:

#+BEGIN_QUOTE
An example in an QUOTE block.
Second line within this block.

This *is* an /example/ of _some_ syntax +highlighting+.
#+END_QUOTE

VERSE blocks are similar to quote blocks, but they respect line breaks:

#+BEGIN_VERSE
An example in an VERSE block.
Second line within this block.

This *is* an /example/ of _some_ syntax +highlighting+.
#+END_VERSE

(Please note that GitLab is ignoring VERSE blocks.)

SRC blocks contain source code snippets. Text formatting is ignored,
line breaks preserved.

A Python source code example:

#+BEGIN_SRC
  def my_test(myvar: str = 'foo bar'):
      """
      This is an example function.

      @type  myvar: str = 'foo bar': number
      @param myvar: str = 'foo bar': FIXXME
      """

      mynewvar: str = myvar + ' additional content'
      return mynewvar

  print("Hello " + my_text('Europe!'))
#+END_SRC

A shell script example:

#+BEGIN_SRC
echo "Hello Europe!"
FOO="foo bar"
echo "A test with ${FOO}"
pwd
#+END_SRC

** Comments

Comment lines contain content which is not visible in any derived
document, such as a PDF document or a web view.

hash space:

# This is a comment

space hash space:

 # This is a comment

space space hash space:

  # This is a comment

-----------

Comment block:

#+BEGIN_COMMENT
This is a multi line comment block.
This is the second line.

This is the second paragraph.

This *is* an /example/ of _some_ syntax +highlighting+.
#+END_COMMENT

** Links

- https://gitlab.com/publicvoit/orgdown → plain URL without brackets
- [[https://gitlab.com/publicvoit/orgdown]] → URL with brackets without description
- [[https://gitlab.com/publicvoit/orgdown][Orgdown homepage]] → URL with brackets with description

** Tables

Tables do not have to be properly aligned. As long as the correct number
of vertical bars per line is met, text formatting is applied.

| *Heading1* | *head2* |
|------------+---------|
| entry      |      42 |
| foo        |    21.7 |
|------------+---------|
| end        |   99.99 |

Don't worry about alignment if you don't have proper tool support. This is a perfectly valid table:

|*Heading1*|*head2*|
|-+--|
|entry|42|
|foo|21.7|
|-+-|
|end|99.99|

More columns:

|     *When* | *What*                 | *USD* | *EUR* |
|------------+------------------------+-------+-------|
| 2012-02-03 | Taxi Graz-Airport      |       | 18.00 |
| 2012-02-03 | Taxi Seattle Airport   | 25.00 | 19.12 |
| 2012-02-13 | Taxi                   |  7.00 |  5.35 |
| 2012-02-14 | Taxi                   |  8.00 |  6.12 |
| 2012-02-17 | Taxi to Airport SeaTac | 35.00 | 26.77 |
| 2012-02-22 | Taxi Airport-Graz      |       | 16.00 |
|------------+------------------------+-------+-------|
|            |                        |       | 91.36 |

Formatting:

| Example                                                 |
|---------------------------------------------------------|
| [[https://gitlab.com/publicvoit/orgdown][Orgdown]]                                                 |
| This *is* an /example/ of _some_ syntax +highlighting+. |

** The Rest
:PROPERTIES:
:CREATED:  [2021-08-11 Wed 11:27]
:END:

All other syntax elements not defined here are interpreted as normal text. Their linebreaks should be respected.

