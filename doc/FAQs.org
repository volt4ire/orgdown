★ [[Overview.org][← Overview page]] ★

* Orgdown FAQs

Here are some frequently asked questions and - much more interesting
to you - hopefully good answers to them.

If you do have a question which should be answered here, please do
hand in a GitLab issue or even a fancy pull request.

** General

- [[doc/faq/name-logo.org][What about this strange name and logo?]]
- [[doc/faq/yet-another-standard.org][Why do we need yet another standard?]]
- [[doc/faq/difference-to-orgmode.org][What is the difference between Org-mode and Orgdown?]]
- [[doc/faq/contribute.org][How can I contribute to Orgdown?]]

** Orgdown1

- [[doc/faq/design-decisions-od1.org][What were the design decisions on Orgdown1 syntax?]]

** Tool Support

- [[doc/faq/unlisted-tool.org][Why is tool X not listed on the tool support page?]]
- [[faq/how-to-add-tools.org][How to add tool X?]]
