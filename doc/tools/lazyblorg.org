★ [[../Tool-Support.org][← Back to Tool Support]] ★ [[../Overview.org][↑ Overview page]] ★

* Orgdown support for lazyblorg

- Platform: Web Page Generators (Python3)
- License: GPL v3
- Price: free
- [[https://github.com/novoid/lazyblorg][Homepage]]
- [[https://github.com/novoid/lazyblorg/issues][Issue management and feature requests]]
- last Orgdown1 syntax assessment: 2022-01-07 using [[https://github.com/novoid/lazyblorg/commit/b71730770aba4fa58890d245f5f9ab30037f3931][commit b717307 from 2022-01-03]] by [[https://Karl-Voit.at][Karl Voit]]
  - See current assessment page: [[https://karl-voit.at/Orgdown1-support/]]

| *Percentage of Orgdown1 syntax support:* | 77 |
#+TBLFM: @1$2=(remote(toolsupport,@>$2)/86)*100;%.0f

** Details

- legend:
  - *1* → works as simple text
  - *2* → at least one of:
    - syntax highlighting
    - support for inserting/collapse/expand/…
    - support for toggling (e.g., checkboxes, states, …)

#+NAME: toolsupport
| Element                                                                     | OD1 | OD1 comment                 |
|-----------------------------------------------------------------------------+-----+-----------------------------|
| syntax elements do not need to be separated via empty lines                 |   0 | Would require [[https://github.com/novoid/lazyblorg/issues/63][proper parser]] |
|-----------------------------------------------------------------------------+-----+-----------------------------|
| *bold*                                                                      |   2 |                             |
| /italic/                                                                    |   1 |                             |
| _underline_                                                                 |   0 | Would require [[https://github.com/novoid/lazyblorg/issues/63][proper parser]] |
| +strike through+                                                            |   2 |                             |
| =code=                                                                      |   2 |                             |
| ~commands~                                                                  |   2 |                             |
| Combinations like *bold and /italic/ text*                                  |   0 | Would require [[https://github.com/novoid/lazyblorg/issues/63][proper parser]] |
| : example                                                                   |   2 |                             |
| [[https://example.com][link *bold* test]]                                                            |   2 |                             |
| [[https://example.com][link /italic/ test]]                                                          |   0 | Would require [[https://github.com/novoid/lazyblorg/issues/63][proper parser]] |
| [[https://example.com][link +strike through+ test]]                                                  |   1 |                             |
| [[https://example.com][link =code= test]]                                                            |   2 |                             |
| [[https://example.com][link ~commands~ test]]                                                        |   2 |                             |
| ≥5 dashes = horizontal bar                                                  |   2 |                             |
|-----------------------------------------------------------------------------+-----+-----------------------------|
| Headings using asterisks                                                    |   2 |                             |
|-----------------------------------------------------------------------------+-----+-----------------------------|
| Unordered list item with "-"                                                |   2 |                             |
| Ordered list item with 1., 2., …                                            |   2 |                             |
| Checkbox + unordered list item with "-"                                     |   2 |                             |
| Checkbox + ordered list item with 1., 2., …                                 |   2 |                             |
| Mixed lists of ordered and unordered items                                  |   2 |                             |
| Multi-line list items with collapse/expand                                  |   2 |                             |
| Multi-line list items with support for (auto-)indentation                   |   2 |                             |
|-----------------------------------------------------------------------------+-----+-----------------------------|
| Example block                                                               |   2 |                             |
| Quote block                                                                 |   2 |                             |
| Verse block                                                                 |   2 |                             |
| Src block                                                                   |   2 |                             |
|-----------------------------------------------------------------------------+-----+-----------------------------|
| Comment lines with "# <foobar>"                                             |   0 |                             |
| Comment block                                                               |   0 | See [[https://github.com/novoid/lazyblorg/issues/65][this feature request]]    |
|-----------------------------------------------------------------------------+-----+-----------------------------|
| https://gitlab.com/publicvoit/orgdown plain URL without brackets            |   2 |                             |
| [[https://gitlab.com/publicvoit/orgdown]] URL with brackets without description |   2 |                             |
| [[https://gitlab.com/publicvoit/orgdown][OD homepage]] URL with brackets with description                              |   2 |                             |
|-----------------------------------------------------------------------------+-----+-----------------------------|
| Basic table functionality                                                   |   2 |                             |
| Table cells with *bold*                                                     |   2 |                             |
| Table cells with /italic/                                                   |   2 |                             |
| Table cells with _underline_                                                |   0 | Would require [[https://github.com/novoid/lazyblorg/issues/63][proper parser]] |
| Table cells with +strike through+                                           |   2 |                             |
| Table cells with =code=                                                     |   2 |                             |
| Table cells with ~commands~                                                 |   2 |                             |
| Auto left-aligning of text                                                  |   2 |                             |
| Auto right-aligning of numbers like "42.23"                                 |   0 | Would require [[https://github.com/novoid/lazyblorg/issues/63][proper parser]] |
|-----------------------------------------------------------------------------+-----+-----------------------------|
| Other syntax elements are interpreted at least as normal text (or better)   |   1 | some may cause errors       |
| Other syntax elements: linebreaks respected (or better)                     |   1 | depends                     |
|-----------------------------------------------------------------------------+-----+-----------------------------|
| *Sum*                                                                       |  66 |                             |
#+TBLFM: @>$2=vsum(@I$2..@>>$2)

